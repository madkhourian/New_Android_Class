package Saeed.Android.App;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("LifeCycle_monitor","onCreate");



    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("LifeCycle_monitor","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("LifeCycle_monitor","onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("LifeCycle_monitor","onstop");
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        Log.d("LifeCycle_monitor","onBackprssed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("LifeCycle_monitor","onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("LifeCycle_monitor","onDestroy");
    }
}
